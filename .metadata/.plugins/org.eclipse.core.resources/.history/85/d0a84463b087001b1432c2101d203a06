package com.strongame.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "user_info")
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;

	@NotNull(message="Name cannot be empty!")
	@Length(min = 2, max = 60)
	@Column(name = "name")
	private String name;

	@NotNull
	@Column(name = "dob")
	private Date userDob;

	@NotNull
	@Length(min = 6, max = 60)
	@Column(name = "email", unique = true)
	private String userEmail;

	@NotNull(message = "Please enter password")
	@Length(min = 4, max = 40)
	@Column(name = "password")
	private String password;

	@NotNull
	@Length(min = 4, max = 40)
	@Column(name = "confirm_password")
	private String confirmPassword;


	@NotNull
	@Column(name = "phone", unique = true)
	private long userPhoneNumber;

	@Column(name = "refer_code")
	private String referalCode;

}
